import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {createSwitchNavigator,createAppContainer,createStackNavigator} from 'react-navigation'
import DrawerNavi from './DrawerNavi';
import LoginStack from './login/LoginStack';
import Splash from './login/Splash';

const SplashST=createStackNavigator({
  Splash:{screen:Splash,navigationOptions:{
    header:null
  }}
})

 const AuthSwitch=createAppContainer(createSwitchNavigator({
   SplashST:SplashST,
    LoginStack:LoginStack,
    DrawerAuth:DrawerNavi
},{
    initialRouteName:'SplashST'
}))
export default AuthSwitch
 
