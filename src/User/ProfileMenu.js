import React, { Component } from 'react'
import { Alert, AsyncStorage, Text, View, SafeAreaView, Image, TouchableOpacity, ScrollView, Switch, BackHandler, StatusBar, ImageBackground } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Styles from '../../Stylesheet';
import HandleBack from '../../HandleBack';
import AnimatedHideView from 'react-native-animated-hide-view';

export default class ProfileMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {

            switch1Value: false,
            Heightt: 0,
            Mode: "Ride Mode",
            DriverMode: global.driverMode,
            MarginnTop: 0,
            marginnBottom: 0,
            thumbC: "#fff",
            BGcolor: "rgba(0, 0, 0, 0)"


        };

    }
    toggleSwitch1 = (value) => {
        // console.warn(this.state.DriverMode)
        this.setState({ switch1Value: value })
        this.state.switch1Value == true ? this.setState({ Heightt: 0, MarginnTop: 0, marginnBottom: 0, Mode: "Ride Mode", thumbC: "#fff", BGcolor: "rgba(0, 0, 0, 0)", DriverMode: "ON" }) :
            this.setState({ Heightt: 50, MarginnTop: 10, marginnBottom: 10, Mode: "Driver Mode", thumbC: '#636FA4', BGcolor: '#636FA4', DriverMode: "OFF" })
        this.ChangeMode();
    }
    ChangeMode() {
        url = global.ConstUrl + 'api/change-mode/' + global.Id
        var formdata = new FormData();
        formdata.append('driver_mode', this.state.DriverMode);
        fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': global.BarCode
            },
            body: formdata
        })
            .then(response => response.json())
            .catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
            .then(response => {
                if (response.status == 200) {
                    console.warn(response.details.driver_mode)
                }
            })

    }
    onBack = () => {
        // Alert.alert(
        //     "",
        //     "Do You Want to Quit App",
        //     [
        //         { text: "No", onPress: () => { }, style: "cancel" },
        //         { text: "Yes", onPress:() => BackHandler.exitApp()},
        //     ],
        //     { cancelable: false },
        // );
        // return true;

    }
    resetUserId() {
        AsyncStorage.removeItem('userDetail')
        console.warn('userDetail successfully removed')
        this.props.navigation.navigate('LoginStack')

    }
    SignOut = () => {
        Alert.alert(
            "",
            "Do You Want to Sign Out",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: "Yes", onPress: () => this.resetUserId() },

            ],
            { cancelable: false },
        );
        return true;

    }
    Settings = () => {

        if (this.state.switch1Value == true) {
            this.props.navigation.navigate('EditProfile')
        } else {
            this.props.navigation.navigate('UserSetting')
        }
    }
    History = () => {

        if (this.state.switch1Value == true) {
            this.props.navigation.navigate('MyRides')
        } else {
            this.props.navigation.navigate('YourRide')
        }
    }
    _Profile = () => {

        if (this.state.switch1Value == true) {
            this.props.navigation.navigate('Dprofile')
        } else {
            this.props.navigation.navigate('Uprofile')
        }
    }
    retrieveData = async () => {
        try {
            var data = await AsyncStorage.getItem('userDetail');
            console.log('stringyfyData', data)
            var userData = JSON.parse(data);
            var userData = JSON.parse(data);
            console.log('unstringyfyData', userData.name)

            if (!global.name || !global.email || !global.Id) {
                global.name = userData.name
                global.email = userData.email
                global.Id = userData.id
                global.driverMode = userData.driver_mode
                global.Profile = global.ConstUrl + 'images/' + userData.image_file
            }

        } catch (error) {
            // Error retrieving data
        }
    };
    componentDidMount() {
        this.retrieveData()
        if (this.state.DriverMode == "ON") {
            this.setState({ switch1Value: true, Heightt: 50, MarginnTop: 10, marginnBottom: 10, Mode: "Driver Mode", thumbC: '#636FA4', BGcolor: '#636FA4', })
            // console.warn(this.state.switch1Value)

        } else {
            this.setState({ switch1Value: false })

        }
    }


    render() {
        return (

            <HandleBack onBack={this.onBack} style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }} >
                    {/* <StatusBar backgroundColor="#636FA4" barStyle="light-content" /> */}
                    <LinearGradient colors={global.ColorG} style={{ flex: 1 }}>
                        <ScrollView showsVerticalScrollIndicator={false}>

                            <TouchableOpacity style={{ flexDirection: 'row', marginTop: 40, marginLeft: 30 }} onPress={this._Profile}>
                                <ImageBackground source={{ uri: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }} style={{ height: 80, width: 80, borderRadius: 100, }}>
                                    <Image source={{ uri: global.Profile }} style={{ height: 80, width: 80, borderRadius: 100 }} />
                                </ImageBackground>
                                <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
                                    <Text style={Styles.ProfileName}>{global.name}</Text>
                                    <Text style={{ color: '#fff' }}>{global.email}</Text>

                                </View>
                            </TouchableOpacity>
                            <View style={{ alignItems: 'flex-end', justifyContent: 'flex-end', paddingRight: 20 }}>
                                <Text style={{ color: '#fff', fontWeight: '500' }}>{this.state.Mode}</Text>
                                <Switch
                                    thumbColor={this.state.thumbC}
                                    trackColor='#fff'
                                    onValueChange={this.toggleSwitch1}
                                    value={this.state.switch1Value}
                                />
                            </View>
                            <View style={{ marginLeft: 20, }}>
                                <AnimatedHideView
                                    visible={this.state.switch1Value}
                                    style={{ backgroundColor: this.state.BGcolor, height: this.state.Heightt, marginTop: this.state.MarginnTop, marginBottom: this.state.marginnBottom, justifyContent: 'center' }}
                                >
                                    <TouchableOpacity style={[Styles.ListItem,]} onPress={() => this.props.navigation.navigate('CreateRide')}>
                                        <Image source={require('../../img/assets/drawer-car.png')} style={{ height: 40, width: 40, marginRight: 20, }} />
                                        <Text style={Styles.ProfileName}>Create Ride</Text>
                                    </TouchableOpacity>

                                </AnimatedHideView>
                                <AnimatedHideView
                                    visible={this.state.switch1Value}
                                    style={{ backgroundColor: this.state.BGcolor, height: this.state.Heightt, marginTop: this.state.MarginnTop, marginBottom: this.state.marginnBottom, justifyContent: 'center' }}
                                >
                                    <TouchableOpacity style={[Styles.ListItem,]} onPress={() => this.props.navigation.navigate('CreateCar')}>
                                        <Image source={require('../../img/assets/drawer-car.png')} style={{ height: 40, width: 40, marginRight: 20, }} />
                                        <Text style={Styles.ProfileName}>Attach Car</Text>
                                    </TouchableOpacity>

                                </AnimatedHideView>
                                <AnimatedHideView
                                    visible={this.state.switch1Value}
                                    style={{ backgroundColor: this.state.BGcolor, height: this.state.Heightt, marginTop: this.state.MarginnTop, marginBottom: this.state.marginnBottom, justifyContent: 'center' }}
                                >
                                    <TouchableOpacity style={[Styles.ListItem]} >
                                        <FontAwesome5 name='hand-holding-usd' color="#fff" size={35} style={{ marginRight: 23 }} />
                                        <Text style={Styles.ProfileName}>Earning</Text>
                                    </TouchableOpacity>

                                </AnimatedHideView>
                                {/* <TouchableOpacity style={[Styles.ListItem]} onPress={() => this.props.navigation.navigate('BookRide')}>
                                    <Image source={require('../../img/assets/drawer-car.png')} style={{ height: 40, width: 40, marginRight: 20 }} />
                                    <Text style={Styles.ProfileName}>Book your ride</Text>
                                </TouchableOpacity> */}
                                <TouchableOpacity style={[Styles.ListItem,]} onPress={this.Settings}>
                                    <FontAwesome name="cog" color="#FFF" size={40} style={{ marginRight: 25, }} />
                                    <Text style={Styles.ProfileName}>Settings</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[Styles.ListItem,]} onPress={this.History}>
                                    <FontAwesome name="history" color="#FFF" size={40} style={{ marginRight: 25 }} />

                                    <Text style={Styles.ProfileName}>History</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.ListItem}>
                                    <Image source={require('../../img/assets/bell-copy.png')} style={{ height: 40, width: 40, marginRight: 20 }} />
                                    <Text style={Styles.ProfileName}>Notifications</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={Styles.ListItem}>
                                    <FontAwesome name="credit-card" color='#fff' size={35} style={{ marginRight: 23 }} />
                                    <Text style={Styles.ProfileName}>Payments</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.ListItem} onPress={() => this.props.navigation.navigate('Support')}>
                                    <Image source={require('../../img/assets/support.png')} style={{ height: 40, width: 40, marginRight: 20 }} />
                                    <Text style={Styles.ProfileName}>Support</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.ListItem} onPress={() => this.props.navigation.navigate('AboutUs')}>
                                    <Image source={require('../../img/assets/About.png')} style={{ height: 40, width: 40, marginRight: 20 }} />
                                    <Text style={Styles.ProfileName}>About</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.ListItem} onPress={this.SignOut}>
                                    <FontAwesome name="sign-out" color='#fff' size={35} style={{ marginRight: 23 }} />
                                    <Text style={Styles.ProfileName}>Sign Out</Text>
                                </TouchableOpacity>

                            </View>


                        </ScrollView>
                    </LinearGradient>
                </SafeAreaView>
            </HandleBack>
        )
    }
}
