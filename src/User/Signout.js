import React, { Component } from 'react'
import { Alert, Text, View, BackHandler } from 'react-native'
import HandleBack from '../../HandleBack';


export default class Signout extends Component {

    onBack = () => {
        Alert.alert(
            "",
            "Are you sure Want to Sign Out",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                {
                    text: "Yes", onPress: () => BackHandler.exitApp(
                    )
                },
            ],
            { cancelable: false },
        );
        return true;

    }
    componentDidMount() {
        this.onBack()
        this.props.navigation.navigate('FindRide')
    }
    render() {
        return (
            <View />
        )
    }
}
