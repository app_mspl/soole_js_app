import React, { Component } from 'react'
import { Text, View, SafeAreaView,ScrollView } from 'react-native'
import Logo from '../Logo';
import LinearGradient from 'react-native-linear-gradient';
import Styles from '../../Stylesheet';

export default class Support extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <LinearGradient colors={global.ColorG} style={{ flex: 1 }}>

                    <View style={[{ flex: 1, }, Styles.AJ]}>
                        <Logo />
                    </View>
                    <View style={[{ flex: 2 }]}>
                    <ScrollView showsVerticalScrollIndicator={false}>

                    
                        <Text style={{ color: '#fff', fontSize: 25, fontWeight: 'bold', textAlign: 'center' }}>
                            Frequently Asked Questions
                      </Text>
                        <View style={{ margin: 20 }}>
                            <Text style={Styles.FaqQst}>
                                What is Lorem Ipsum?
                      </Text>
                            <Text style={{ color: '#fff' }}>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                      </Text>
                        </View>
                        <View style={{ margin: 20 }}>
                            <Text style={Styles.FaqQst}>
                            Why do we use it?
                      </Text>
                            <Text style={{ color: '#fff' }}>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                      </Text>
                        </View>
                        <View style={{ margin: 20 }}>
                            <Text style={Styles.FaqQst}>
                            Where does it come from?
                      </Text>
                            <Text style={{ color: '#fff' }}>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                      </Text>
                        </View>
                        </ScrollView>
                    </View>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}
