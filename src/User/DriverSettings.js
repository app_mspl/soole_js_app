import React, { Component } from 'react'
import { Text, View, SafeAreaView, Image, TouchableOpacity,ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Styles from '../../Stylesheet';
export default class DriverSettings extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
             <LinearGradient colors={global.ColorG} style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
               
                    <View style={{ flexDirection: 'row', marginTop: 40, marginLeft: 30 }}>
                        <FontAwesome name="user-circle-o" size={80} color="#fff" />
                        <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
                            <Text style={Styles.ProfileName}>Jonathan Lee</Text>
                            <Text style={{ color: '#fff' }}>jonathanlee@mail.com</Text>

                        </View>
                    </View>
                    <View style={{marginLeft:20,marginTop:30}}>
                        <TouchableOpacity style={Styles.ListItem} >
                                <Image source={require('../../img/assets/drawer-car.png')} style={{height:40,width:40,marginRight:20}}/>
                                <Text style={Styles.ProfileName}>Earning</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={Styles.ListItem}>
                                <Image source={require('../../img/assets/bell-copy.png')} style={{height:40,width:40,marginRight:20}}/>
                                <Text style={Styles.ProfileName}>History</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={Styles.ListItem} onPress={()=> this.props.navigation.navigate('UserSetting')}>
                                <FontAwesome name="cog" color="#FFF" size={40} style={{marginRight:25}}/>
                                <Text style={Styles.ProfileName}>Settings</Text>
                        </TouchableOpacity>
                        
                       
                        <TouchableOpacity style={Styles.ListItem}>
                                <FontAwesome name="credit-card" color='#fff' size={35} style={{marginRight:23}}/>
                                <Text style={Styles.ProfileName}>Sign Out</Text>
                        </TouchableOpacity>
                        
                    </View>
                
                </ScrollView>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}
