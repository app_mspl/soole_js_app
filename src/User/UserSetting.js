import React, { Component } from 'react'
import { Text, View, SafeAreaView, TextInput, Button, Image, TouchableOpacity,ScrollView } from 'react-native'
import Styles from '../../Stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AnimatedHideView from 'react-native-animated-hide-view'
export default class UserSetting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            CardText: '#636FA4',
            CashText: '#000',
            BW: 2,
            BW1: 0,
            Heightt: 120,
            Visible:true,
            ButtonPay:'PAY NOW'
            

        };
    }
    onButtonPress = () => {
        this.setState({ CardText: '#000', CashText:'#636FA4',BW: 0,BW1:2,Heightt:0,Visible:false,ButtonPay:'PAY LATER'  });
    }
    onButtonPress1= () => {
        this.setState({ CardText: '#636FA4', CashText: '#000',BW: 2,BW1:0,Heightt:120,Visible:true ,ButtonPay:'PAY NOW'  });
    }
    render() {
        return ( 
            <SafeAreaView style={{ flex: 1 }}>
                <View style={Styles.ProfileHeader}>
                    <Text style={{ color: '#fff', fontWeight: '500' }}>User Setting</Text>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ backgroundColor: '#fff', elevation: 10, marginLeft: 20, marginRight: 20, marginTop: 40 }}>
                    <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#636FA4', paddingLeft: 20, alignItems: 'center' }}>
                        <Image source={require('../../img/assets/drawer-car.png')} style={{ height: 40, width: 60 }} />
                        <Text style={{ color: '#fff', fontWeight: '500', marginLeft: 20 }}>Pay $45</Text>
                    </View>
                    <View style={{ margin: 30 }}>
                        <View>
                            <Text style={{ color: '#636FA4', fontWeight: '500', }}>Address</Text>
                            <Text style={{ fontWeight: '500', maxWidth: 150 }}>9879 Sega Court Roselie, IL 60172</Text>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <TextInput
                                placeholder='Enter your email'
                                keyboardType='email-address'
                                style={{ borderBottomWidth: 1, borderBottomColor: '#d3d3d3' }}
                            />
                            <TextInput
                                placeholder='Mobile number'
                                keyboardType='phone-pad'
                                style={{ borderBottomWidth: 1, borderBottomColor: '#d3d3d3' }}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <TouchableOpacity style={{ flex: 1, flexDirection: 'row' }} onPress={this.onButtonPress1}>
                                <FontAwesome name='credit-card' size={20} color={this.state.CardText} />
                                <Text style={{ fontWeight: '500', color:this.state.CardText, borderBottomWidth:this.state.BW, borderBottomColor: '#636FA4' }}>
                                Cards
                                </Text>
                            </TouchableOpacity>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity onPress={this.onButtonPress} style={{flex:1,flexDirection:'row'}}>
                                <Text style={{ fontWeight: '500',color:this.state.CashText,borderBottomWidth:this.state.BW1,borderBottomColor: '#636FA4' }}>
                                Cash</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <AnimatedHideView visible={this.state.Visible} style={{ marginTop: 20,height:this.state.Heightt,}}>
                            <TextInput
                                placeholder='xxxx xxxx xxxx xxxx'
                                keyboardType='number-pad'
                                style={{ borderWidth: 1, paddingLeft: 10, borderColor: '#d3d3d3' }}
                            />
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <TextInput
                                    placeholder='Entry Date'
                                    
                                    style={{ flex: 2, borderWidth: 1, paddingLeft: 10, borderColor: '#d3d3d3' }}
                                />
                                <TextInput
                                    placeholder='ccv'
                                    keyboardType='number-pad'
                                    style={{ flex: 1, borderWidth: 1, textAlign: 'center', marginLeft: 10, borderColor: '#d3d3d3' }}
                                />
                            </View>
                        </AnimatedHideView>
                        <View style={{marginTop:20}}>
                        <Button title={this.state.ButtonPay} color='#636FA4' />
                        </View>
                    </View>
                </View>
                <View style={{height:50}}>

                </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}
