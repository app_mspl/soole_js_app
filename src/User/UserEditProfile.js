import React, { Component } from 'react'
import { Alert, Text, View, SafeAreaView, Image, TextInput, Button, ScrollView, CameraRoll, TouchableOpacity, Modal, ActivityIndicator, ImageBackground } from 'react-native'
import Styles from '../../Stylesheet'
//import PhotoUpload from 'react-native-photo-upload'
// import ImagePicker from 'react-native-customized-image-picker';
import ImagePicker from 'react-native-image-picker'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
export default class UserEditProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            Base64: '',
            name: '',
            mobileNo: '',
            profilePic: global.ConstUrl + 'images/' + global.Img,
            animate: false,

        }
        this.state.name = global.name || null
        this.state.mobileNo = global.phone || null
    }
    SucessMessage = () => {
        Alert.alert(
            "",
            " Profile Update sucessfully",
            [
                { text: "OK", onPress: () => this.props.navigation.navigate('FindRide') },
            ],
            { cancelable: false },
        );
        return true;

    }
    _Submit = () => {
        console.warn(this.state)
        this.setState({
            animate: true
        })
        const profilePicUrl = this.state.profilePic

        var url = global.ConstUrl + 'api/update-profile/' + global.Id
        var formdata = new FormData();
        formdata.append('name', this.state.name);
        formdata.append('phone', this.state.mobileNo);
        formdata.append('image_file', { uri: profilePicUrl, name: 'image.jpg', type: 'multipart/form-data' });
        console.warn(url)
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': global.BarCode
            },
            body: formdata
        })
            .then(response => response.json())
            .catch(error => {
                console.warn('Error:', error)
                this.setState({
                    animate: false
                })
            })
            .then(response => {
                console.warn('user Data:', response)
                // console.warn(response.error)
                if (response.status == 200) {
                    this.setState({
                        animate: false
                    })
                    global.name = this.state.name,
                        global.phone = this.state.mobileNo
                    this.SucessMessage()

                }
                else {
                    if (response.error.hasOwnProperty('name')) {
                        let x = response.error.name;
                        alert((x).toString())
                        this.setState({
                            animate: false
                        })
                    }
                    else if (response.error.hasOwnProperty('image_file')) {
                        let x = response.error.image_file;
                        alert((x).toString())
                    }
                    else {
                        console.warn(response.error)

                    }

                }
            })
    }
    profilePickerHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 200, maxHeight: 200 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    profilePic: res.uri
                });
                global.Profile = this.state.profilePic

            }
        });
    }

    render() {
        // global.ProfileImg=this.state.profilePic
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={Styles.HeaderRide}>
                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                            <FontAwesome name="bars" size={30} color="#fff" />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                            <Text style={{ color: '#fff', fontWeight: '500', }}>
                                Edit Profile</Text>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <View style={[Styles.AJ]}>
                                <TouchableOpacity onPress={this.profilePickerHandler}>
                                    <ImageBackground source={{ uri: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }} style={{ height: 150, width: 150, borderRadius: 100, }}>
                                        <Image
                                            style={{
                                                paddingVertical: 30,
                                                width: 150,
                                                height: 150,
                                                borderRadius: 75,
                                                backgroundColor: '#d3d3d3'
                                            }}
                                            resizeMode='cover'
                                            source={{ uri: this.state.profilePic }}
                                        />
                                    </ImageBackground>
                                </TouchableOpacity>
                                {/* <Image source={require('../../img/profile_user.jpg')} style={{ height: 150, width: 150, borderRadius: 100 }} /> */}
                                <Text style={Styles.Name}>{global.name}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1.5, margin: 20 }}>
                            <TextInput
                                placeholder='Name'
                                onChangeText={(name) => this.setState({ name })} defaultValue={global.name}
                                returnKeyType='next'

                                style={[Styles.LoginText, { marginTop: 10 }]}
                            />
                            <TextInput
                                placeholder='Mobile No'
                                keyboardType='numeric'
                                onChangeText={(mobileNo) => this.setState({ mobileNo })} defaultValue={global.phone}
                                returnKeyType='next'
                                style={[Styles.LoginText, { marginTop: 20 }]}
                            />
                            <TextInput
                                placeholder='Email'
                                editable={false}
                                keyboardType='email-address'
                                defaultValue={global.email}
                                returnKeyType='next'
                                style={[Styles.LoginText, { marginTop: 20 }]}
                            />


                            <View style={{ marginTop: 30 }}>
                                <Button title="SUBMIT" color="#636FA4" onPress={this._Submit} />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <Modal visible={this.state.animate} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.animate}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff"
                            />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </SafeAreaView>
        )
    }
}
