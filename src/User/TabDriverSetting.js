import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {createMaterialTopTabNavigator,createAppContainer} from 'react-navigation'
import UserSetting from './UserSetting'
import EditProfile from './EditProfile'

const DriverTab=createMaterialTopTabNavigator({
    UserSetting:{
        screen:UserSetting,navigationOptions:{
            title:'User Settings'
        }
    },
    EditProfile:{
        screen:EditProfile,navigationOptions:{
            title:'Driver Settings'
        }
    }
    },
    {
        tabBarOptions:{
            activeTintColor: '#636FA4',
            inactiveTintColor:'gray',
            indicatorStyle:{
                backgroundColor:'#636FA4'
            },
            
            style: {
                backgroundColor: '#fff',
              },
        }
    }
)
const TabDriver=createAppContainer(DriverTab)

export default class TabDriverSetting extends Component {
  render() {
    return (
      <TabDriver/>
    )
  }
}
