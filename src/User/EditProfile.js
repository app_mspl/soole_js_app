import React, { Component } from 'react'
import { Text, View, SafeAreaView, ScrollView, TextInput, Image, Switch, TouchableOpacity, ImageBackground } from 'react-native'
import Styles from '../../Stylesheet'
import ImagePicker from 'react-native-image-picker'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
export default class EditProfile extends Component {
  constructor() {
    super();
    this.state = {
      switch1Value: false,
      RegNo: "DL 1A 0000",
      licenceNo: "123456789",
      pickedImage: global.ConstUrl + 'images/' + global.Img,
    }
  }
  toggleSwitch1 = (value) => {
    this.setState({ switch1Value: value }) //condition ? if : else
    value == true ? this.setState({ RegNo: 'DL 1A 0000' }) : this.setState({ RegNo: '' })

    // console.warn('Switch 1 is: ' + value)
  }
  pickImageHandler = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 200, maxHeight: 200 }, res => {
      if (res.didCancel) {
        console.log("User cancelled!");
      } else if (res.error) {
        console.log("Error", res.error);
      } else {
        this.setState({
          pickedImage: res.uri
        });
        global.Profile = this.state.pickedImage

      }
    });
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={Styles.HeaderRide}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
            <FontAwesome name="bars" size={30} color="#fff" />
          </TouchableOpacity>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
            <Text style={{ color: '#fff', fontWeight: '500', }}>
              Driver Setting</Text>
          </View>
          <View style={{ flex: 1 }} />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ flexDirection: 'row', margin: 10, }}>
            <View style={{ flex: 1 }} />
            <View style={[Styles.AJ, { flex: 3, }]}>
              <ImageBackground source={{ uri: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }} style={{ height: 150, width: 150, borderRadius: 100, }}>
                <Image source={{ uri: global.Profile }} style={{ height: 150, width: 150, borderRadius: 100 }} />
              </ImageBackground>
              <Text style={Styles.Name}>{global.name}</Text>
            </View>
            <View style={{ flex: 1 }}>

            </View>
          </View>
          <View style={{ height: 90, flexDirection: 'row', elevation: 10, backgroundColor: '#636FA4' }}>
            <View style={[Styles.AJ, { flex: 1 }]}>
              <Text style={Styles.RatingTxt}>4.77</Text>
              <Text style={{ color: '#fff' }}>Rating</Text>
            </View>
            <TouchableOpacity style={[Styles.AJ, { flex: 1 }]} onPress={() => this.props.navigation.navigate('MyRides')}>
              <Text style={Styles.RatingTxt}>2</Text>
              <Text style={{ color: '#fff' }}>Rides</Text>
            </TouchableOpacity>

          </View>
          <View style={[Styles.AJ, { margin: 20 }]}>
            <View style={{ flexDirection: 'row', borderWidth: 2, borderColor: '#636FA4', height: 40, minWidth: 175, borderRadius: 5, alignItems: 'center', paddingLeft: 5 }}>
              <Text>REG NO:  </Text>
              <Text>{this.state.RegNo}</Text>
            </View>
          </View>

          <View style={{ marginLeft: 20, marginRight: 20 }}>
            <View style={Styles.ProfileDetail}>
              <Text style={Styles.Name}>Owner Name</Text>
              <Text>JOHN ARDEN</Text>
            </View>
            <View style={Styles.ProfileDetail}>
              <Text style={Styles.Name}>Registration Date</Text>
              <Text>15-DEC-2010</Text>
            </View>
            <View style={Styles.ProfileDetail}>
              <Text style={Styles.Name}>Vehicle Age</Text>
              <Text>5 year 8 months Old</Text>
            </View>
            <View style={Styles.ProfileDetail}>
              <Text style={Styles.Name}>Vehicle Type</Text>
              <Text>Maruti Sazuki pvt ltd, sedan</Text>
            </View>
            <View style={Styles.ProfileDetail}>
              <Text style={Styles.Name}>Vehicle Number</Text>
              <Text>CBQ 796</Text>
            </View>
            <View style={Styles.ProfileDetail}>
              <Text style={Styles.Name}>Road Worthiness</Text>
              <Text>Good</Text>
            </View>
            <View style={[Styles.ProfileDetail, { flexDirection: 'row' }]}>
              <View>
                <Text style={[Styles.Name, { marginBottom: 5, marginTop: 5 }]}>Licence</Text>
                <Text>Upload your Licence</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 10, justifyContent: 'center' }}>
                <TouchableOpacity onPress={this.pickImageHandler}>
                  <FontAwesome name="upload" size={25} color="#000" />
                </TouchableOpacity>
              </View>

            </View>
            {/* <View style={Styles.ProfileDetail}>
              <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: "row", justifyContent: 'flex-start' }}>
                  <Text style={Styles.Name}>Licence No</Text>
                  <TextInput
                    placeholder="Licence No" keyboardType='numeric'
                    selectTextOnFocus={false}
                    defaultValue={this.state.licenceNo}
                  />
                </View>
                <View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
                  <TouchableOpacity onPress={this.pickImageHandler} style={{
                    marginBottom: 10,

                  }}>
                    <ImageBackground source={require('../assets/Upload_Document-512.png')} style={{ height: 75, width: 75, }}>
                      <Image
                        style={{
                          // paddingVertical: 30,
                          // width: 150,
                          // height: 150,
                          // borderRadius: 75,
                          backgroundColor: '#d3d3d3'
                        }}
                        resizeMode='cover'
                        source={{ uri: this.state.pickedImage }}
                      />
                    </ImageBackground>
                  </TouchableOpacity>
                </View>
              </View>
            </View> */}
          </View>
        </ScrollView>
      </SafeAreaView >
    )
  }
}
