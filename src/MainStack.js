import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import Menu from './User/ProfileMenu'

const Stack = createStackNavigator({
  Menu: {
    screen: Menu, navigationOptions: {
      header: null
    }
  },
})
const MenuStack = createAppContainer(Stack)
export default class MainStack extends Component {
  render() {
    return (
      <MenuStack />
    )
  }
}
