import React, { Component } from 'react'
import { Text, View, Dimensions } from 'react-native'
import { createAppContainer, createDrawerNavigator } from 'react-navigation'
import ProfileMenu from './User/ProfileMenu';
import MyRides from './booking/MyRides'
import FindRide from './booking/FindRide'
import UserSetting from './User/UserSetting'
import EditProfile from './User/EditProfile'
import Uprofile from './User/UserEditProfile'
import Dprofile from './User/DriverEditProfile'
import BookRide from './booking/BookRide';
import YourRide from './booking/YourRide'
import CreateRide from './booking/CreateRide';
import Signout from './User/Signout';
import AboutUs from './User/AboutUs'
import Support from './User/Support';
import CreateCar from './booking/CreateCar';
import AvailableCar from './booking/AvailableCar';

const screenWidth = Dimensions.get('window').width;
const MyDrawerNavigator = createDrawerNavigator({
  FindRide: {
    screen: FindRide, navigationOptions: {
      title: 'Find a Ride',
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#636FA4',
      },
    },
  },

  UserSetting: {
    screen: UserSetting, navigationOptions: {
      header: null
    }
  },
  CreateRide: {
    screen: CreateRide, navigationOptions: {
      header: null
    }
  },
  YourRide: {
    screen: YourRide, navigationOptions: {
      header: null
    }
  },
  BookRide: {
    screen: BookRide, navigationOptions: {
      header: null
    }
  },
  CreateCar: {
    screen: CreateCar, navigationOptions: {
      header: null
    }
  },
  Uprofile: {
    screen: Uprofile, navigationOptions: {
      header: null
    }
  },

  Dprofile: {
    screen: Dprofile, navigationOptions: {
      header: null
    }
  },
  MyRides: {
    screen: MyRides, navigationOptions: {
      header: null
    }
  },
  EditProfile: {
    screen: EditProfile, navigationOptions: {
      header: null
    }
  },
  AboutUs: {
    screen: AboutUs, navigationOptions: {
      header: null
    }
  },
  Support: {
    screen: Support, navigationOptions: {
      header: null
    }
  },
  Signout: {
    screen: Signout, navigationOptions: {
      header: null
    }
  },
  AvailableCar: {
    screen: AvailableCar, navigationOptions: {
      header: null
    }
  },
},
  {
    initialRouteName: 'FindRide',
    //    drawerWidth:screenWidth,
    contentOptions: {
      style: {
        backgroundColor: 'black',
        flex: 1,


      }
    },
    navigationOptions: {
      drawerLockMode: 'locked-closed'
    },
    contentComponent: ProfileMenu,
  },
  // {
  //   initialRouteName: 'AvailableCar',
  //   //    drawerWidth:screenWidth,
  //   contentOptions: {
  //     style: {
  //       backgroundColor: 'black',
  //       flex: 1,

  //     }
  //   },
  //   navigationOptions: {
  //     drawerLockMode: 'locked-closed'
  //   },
  //   contentComponent: ProfileMenu,
  // }
);

export default MyApp = createAppContainer(MyDrawerNavigator);