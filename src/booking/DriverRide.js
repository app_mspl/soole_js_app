import React, { Component } from 'react'
import { Text, View, SafeAreaView, ScrollView, Image, TouchableOpacity } from 'react-native'
import Styles from '../../Stylesheet';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MapComponent from '../Gmap/GoogleMap';
export default class DriverRide extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={Styles.header}>
                    <Text style={{ color: '#fff', fontWeight: '500' }}>Ride History</Text>
                </View>
                <View style={[{ flex: 1, }]}>
                    <MapComponent />
                    <View style={{margin:20,backgroundColor:'#fff',padding:20}}>
                        <View style={{flexDirection:'row'}}>
                        <Image source={require('../../img/profile_user.jpg')} style={{ height: 100, width: 100, borderRadius: 100 }} />
                            <View>
                                <Text style={{color:'#636FA4',fontWeight:'500'}}>John Arden</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
