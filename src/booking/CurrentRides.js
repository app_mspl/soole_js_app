import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity,Button } from 'react-native'
import Styles from '../../Stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
export default class CurrentRides extends Component {
    render() {
        const pattren = '<.........>'
        return (

            <View style={{ flex: 1, margin: 20 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{backgroundColor:'#636FA4',height:50,marginTop:20,marginBottom:20,borderRadius:3,justifyContent:'center',paddingLeft:20}}>
                        <Text style={{color:'#FFF',fontWeight:'500'}}>YOUR OFFERED RIDES</Text>
                    </View>
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Tue. 22jan -12:20
                       </Text>
                            <Text>
                                Avenue Elizabeth city{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Bethesda MD
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>3 seats Available</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Wed. 23jan -16:00
                       </Text>
                            <Text>
                                Bethesda MD{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Avenue Elizabeth city
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>3 seats Available</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View><View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Thus. 24jan -18:00
                       </Text>
                            <Text>
                                Avenue Elizabeth city{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Bethesda MD
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>3 seats Available</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{marginTop:30}}>
                        <Button title="SCHEDULE A RIDE " color="#636FA4"/>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
