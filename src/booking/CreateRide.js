import React, { Component } from 'react'
import { Alert, Text, View, Picker, SafeAreaView, TouchableOpacity, ScrollView, TextInput, ActivityIndicator, DatePickerAndroid, TimePickerAndroid, Modal } from 'react-native'
import Styles from '../../Stylesheet';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import DatePicker from 'react-native-datepicker-latest'
var hours = new Date().getHours(); //Current Hours
var min = new Date().getMinutes(); //Current Minutes
var date = new Date().getDate(); //Current Date
var month = new Date().getMonth() + 1; //Current Month
var year = new Date().getFullYear(); //Current Year
export default class CreateRide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Route: '',
            Car: '',
            Seats: '',
            Time: '',
            data1: null,
            data2: [{pick_title:"",id:"",drop_title:"",route_id:''}],
            isLoading: false,
            curTime: null,
            curDate: null,
            animate: false,
            testt: null,
            Time_:null,
            Id_route:null,
            pick_up:'',
            drop_off:'',
        
        }
    }


    componentDidMount() {
        this._BoardingPoint()
        let monthSet = month <= 9 ? "0" + month : month;
        let dateSet = date <= 9 ? "0" + date : date;
        let x = min <= 9 ? "0" + min : min;
        let y = hours <= 9 ? "0" + hours : hours;

        this.setState({ curTime: y + ":" + x, curDate: year + "-" + monthSet + "-" + dateSet });
        
        url = global.ConstUrl + 'api/route-list'
        fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': global.BarCode
            },
        })
            .then(response => response.json())
            .catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
            .then(response => {
                if (response.success == true) {
                    this.setState({
                        animate: false
                    })
                    this.setState({ data1: response.data,})
                }
                else {

                }
            })

    }
    _BoardingPoint=(value, index)=> {
        // console.warn(value)
        this.setState({
            Id_route:value,
            animate: true
        })
        url = global.ConstUrl + 'api/boardings/'+this.state.Id_route
console.warn(this.state.Id_route)


        fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': global.BarCode
            },
        })
            .then(response => response.json())
            .catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
            .then(response => {
                this.setState({
                    animate: false
                })
                if (response.success == true) {
                    this.setState({ data2: response.data, animate: false })
                    // console.warn(response)
                    console.warn(this.state.data2)
                }

            })

    }
    async openAndroidDatePicker() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date()
            });
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    }
    async openAndroidTimePicker() {

        try {
            const { action, hour, minute } = await TimePickerAndroid.open({
                hour: 14,
                minute: 0,
                is24Hour: false,

            });
            if (action !== TimePickerAndroid.dismissedAction) {
                // Selected hour (0-23), minute (0-59)
                var x = minute <= 9 ? "0" + minute : minute;
                var y = hour <= 9 ? "0" + hour : hour;
                this.setState({ testt: y + ":" + x, });
            }

        } catch ({ code, message }) {

            console.warn('Cannot open time picker', message, '---------', code);
        }
    }

    _formatTime(hour, minute) {
        return hour + ':' + (minute < 10 ? '0' + minute : minute);
    }
    SucessMessage = () => {
        Alert.alert(
            "Congratulation",
            " DRIVE created sucessfully",
            [
                { text: "OK", onPress: () => this.props.navigation.navigate('MyRides') },
            ],
            { cancelable: false },
        );
        return true;

    }
    RouteUD = () => {
        // alert('Ride Created Sucessfully')
        this.setState({
            animate: true
        })
        url = global.ConstUrl + 'api/create-ride'
        const route = this.state.Id_route
        const car = this.state.Car
        const seats = this.state.Seats
        const time = this.state.curTime
        const date_ = this.state.curDate

        var formdata = new FormData();

        formdata.append('route_id', route);
        formdata.append('car_id', "2");
        formdata.append('available_seats', "4");
        formdata.append('created_by_id', "24");
        formdata.append('start_time', date_ + " " + time);
        formdata.append('pick_drop_id', route);



        fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': global.BarCode
            },
            body: formdata

        })
            .then(response => response.json())
            .catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
            .then(response => {
                this.setState({
                    animate: false
                })
                if (response.success == true) {
                    this.SucessMessage()
                }
                else{
                    console.warn(response.error)
                }

            })

    }


    render() {
        // setInterval(function(){this.setState({curTime: hours +":"+ min});}.bind(this), 1000);

        const Pro = this.state.data1
        // const Pro2 = this.state.data2
        if (!Pro) {
            return (
                <ActivityIndicator
                    animating={true}
                    style={Styles.indicator}
                    size='large'
                />
            );
        }
        // else if (!Pro2) {
        //     return (
        //         <ActivityIndicator
        //             animating={true}
        //             style={Styles.indicator}
        //             size='large'
        //         />
        //     );
        // }
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={Styles.HeaderRide}>
                <TouchableOpacity style={{flex:1,justifyContent:'center'}}onPress={()=> this.props.navigation.openDrawer()}>
                    <FontAwesome name="bars" size={30} color="#fff"/>
                </TouchableOpacity>
                    <View style={{justifyContent: 'center',alignItems:'center' ,flex:2,}}>
                    <Text style={{ color: '#fff', fontWeight: '500', }}>
                    Create Drive</Text>
                    </View>
                    <View style={{flex:1}}/>
            </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ margin: 30, backgroundColor: '#FFF', elevation: 10, padding: 20 }}>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Route</Text>
                                <Picker selectedValue={this.state.Id_route} onValueChange={this._BoardingPoint} >
                                     
                                    {Pro.map((item, key) => (
                                        <Picker.Item label={item.title} value={item.id} key={item.id} />)
                                    )}
                                    
                                </Picker>
                            </View>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Pickup Point</Text>
                                <Picker selectedValue={this.state.pick_up} onValueChange={(pick_up) => this.setState({ pick_up })}>
                                    {this.state.data2.map((item, key) => (
                                        <Picker.Item label={item.pick_title} value={item.id} key={item.id} />)
                                    )}
                                </Picker>
                            </View>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Dropoff Point</Text>
                                <Picker selectedValue={this.state.pick_up} onValueChange={(pick_up) => this.setState({ pick_up })}>
                                {this.state.data2.map((item, key) => (
                                        <Picker.Item label={item.drop_title} value={item.id} key={item.id} />)
                                    )}
                                </Picker>
                            </View>
                            <View style={{ marginBottom: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: "500", marginLeft: 10 }}>Select Time</Text>
                                <Text style={{color:"red",marginLeft:10}}>*you can select time only next 4 hours from current time </Text>
                                <View style={{ flexDirection: 'column' }}>
                                    {/* <TouchableOpacity style={[]} >
                                        <DatePicker
                                            style={[Styles.DateTime, Styles.AJ]}
                                            date={this.state.curDate}
                                            mode="date"
                                            placeholder="select date"
                                            format="YYYY-MM-DD"
                                            minDate="2019-02-26"
                                            
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 4,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 36
                                                }
                                                // ... You can check the source to find the other keys.
                                            }}
                                            onDateChange={(date) => { this.setState({ curDate: date }) }}
                                        />
                                        <Text style={{ color: "#000", fontWeight: '500' }}>{this.state.curDate}</Text> 
                                    </TouchableOpacity> */}
                                    <TouchableOpacity style={[]}>
                                    <DatePicker
                                            style={[Styles.DateTime, Styles.AJ]}
                                            date={this.state.curTime}
                                            mode="time"
                                            placeholder="select time"
                                            // format="h:mm:ss a"
                                            // minDate="2019-02-26"
                                            
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 4,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 36
                                                }
                                                // ... You can check the source to find the other keys.
                                            }}
                                            onDateChange={(date) => { this.setState({ curTime: date }) }}
                                        />
                                        {/* <Text style={{ color: "#000", fontWeight: '500' }}>{this.state.curTime}</Text> */}
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <TouchableOpacity style={[Styles.AJ, { height: 50, backgroundColor: '#636FA4' }]} onPress={this.RouteUD}>
                                <Text style={{ color: "#fff", fontWeight: '500' }}>CREATE DRIVE</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
                <Modal visible={this.state.animate} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.animate}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff"
                            />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>

                </Modal>
            </SafeAreaView>
        )
    }
}
