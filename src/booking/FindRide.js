import React, { Component } from 'react'
import { Alert, Text, View, TextInput, SafeAreaView, StatusBar, TouchableOpacity, Image, ScrollView, Dimensions, BackHandler } from 'react-native'
import Styles from '../../Stylesheet'
import { RNNumberStepper } from 'react-native-number-stepper';
import MapView, { Marker, } from 'react-native-maps';
import MapComponent from '../Gmap/GoogleMap';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import HandleBack from '../../HandleBack';
import DatePicker from 'react-native-datepicker-latest'
export default class FindRide extends Component {
    constructor(props) {
        super(props);

        this.state = {
            RideText: '#636FA4',
            DriveText: '#000',

        };
    }
    onBack = () => {
        Alert.alert(
            "",
            "Do You Want to Quit The App?",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: "Yes", onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        );
        return true;

    }

    onButtonPress = () => {
        this.setState({ RideText: '#000', DriveText: '#636FA4' });
    }
    onButtonPress1 = () => {
        this.setState({ DriveText: '#000', RideText: '#636FA4' });

    }
    render() {
        return (
            <HandleBack onBack={this.onBack} style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    {/* <StatusBar backgroundColor="#636FA4" barStyle="light-content" /> */}
                    <View style={Styles.HeaderRide}>
                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                            <FontAwesome name="bars" size={30} color="#fff" />
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                            <Text style={{ color: '#fff', fontWeight: '500', }}>
                                Book Ride</Text>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={[{ flex: 1, backgroundColor: '#d3d3d3', minHeight: 300 }]}>
                            <MapComponent />

                        </View>
                        <View style={{ flex: 1, margin: 20, }}>
                            <View style={{ flex: 1, backgroundColor: '#fff', elevation: 5, top: -50 }}>

                                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                    <View style={{ flex: 1 }} >
                                        <Text style={{ color: this.state.RideText, fontWeight: '500', textAlign: 'center', }}>RIDE</Text>
                                    </View>
                                    {/* <TouchableOpacity style={{ flex: 1 }} onPress={this.onButtonPress}>
                                <Text style={{ color: this.state.DriveText, fontWeight: '500', textAlign: 'left', paddingLeft: 20, }}>DRIVE</Text>
                            </TouchableOpacity> */}
                                </View>

                                <View>
                                    <View >
                                        <View style={[{
                                            flexDirection: 'row', justifyContent: 'center', justifyContent: 'center',
                                            backgroundColor: '#fff',
                                            height: 60,

                                        }]}>
                                            <View>
                                                <Text style={{ marginLeft: 60, marginTop: 15, fontSize: 20, color: 'black', fontWeight: '100' }}>Ride At</Text>

                                            </View>

                                            <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 10, justifyContent: 'center' }}>
                                                <TouchableOpacity >
                                                    <DatePicker
                                                        style={[Styles.AJ, {
                                                            borderWidth: 1,
                                                            borderColor: '#636FA4',
                                                            borderRadius: 10,
                                                            width: 180
                                                        }]}
                                                        date={this.state.curTime}
                                                        mode="time"
                                                        placeholder="select time"
                                                        // format="h:mm:ss a"
                                                        // minDate="2019-02-26"

                                                        confirmBtnText="Confirm"
                                                        cancelBtnText="Cancel"
                                                        customStyles={{
                                                            dateIcon: {
                                                                position: 'absolute',
                                                                left: 0,
                                                                top: 4,
                                                                marginLeft: 0
                                                            },
                                                            dateInput: {
                                                                marginLeft: 36
                                                            }
                                                            // ... You can check the source to find the other keys.
                                                        }}
                                                        onDateChange={(date) => { this.setState({ curTime: date }) }}
                                                    />
                                                    {/* <Text style={{ color: "#000", fontWeight: '500' }}>{this.state.curTime}</Text> */}
                                                </TouchableOpacity>
                                            </View>

                                        </View>

                                    </View>
                                </View>
                                <View style={[Styles.FindRideView,]} >

                                    <View style={[Styles.AJ, { flex: 1 }]}>
                                        <Text style={{ fontWeight: '500' }}>Avenue Elizabath</Text>
                                        <Text style={{ color: "#636FA4", fontSize: 25, fontWeight: '500' }}>45 mins</Text>
                                    </View>
                                    <View style={[Styles.AJ, { flex: 1, justifyContent: 'flex-start' }]}>
                                        <Text style={{ fontWeight: '500' }}>Seats</Text>
                                        <View>
                                            <RNNumberStepper
                                                size={1}
                                                value={1}
                                                minValue={1}
                                                maxValue={4}
                                                autoRepeat={false}
                                                buttonsBackgroundColor='#fff'
                                                buttonsTextColor='#000'
                                                labelBackgroundColor='#fff'
                                                labelTextColor='#000'
                                                borderWidth={1}
                                                borderColor='#636FA4'
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={{ margin: 40 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image source={require('../../img/visa.png')} style={{ height: 15, width: 40 }} />
                                        <Text style={{ fontWeight: '500', marginLeft: 10 }}>$0.00 credits</Text>
                                    </View>
                                    <TouchableOpacity style={[Styles.AJ, { backgroundColor: '#636FA4', borderRadius: 3, height: 50, marginTop: 5 }]}
                                        onPress={() => this.props.navigation.navigate('AvailableCar')}>
                                        <Text style={{ fontWeight: "500", color: '#fff' }}>
                                            Reserve Spot $125
                                    </Text>
                                        <Text style={{ color: '#fff', fontSize: 10 }}>Join the queue</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </HandleBack>
        )
    }
}
