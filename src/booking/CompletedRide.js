import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity,Button } from 'react-native'
import Styles from '../../Stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
export default class CompletedRide extends Component {
    render() {
        const pattren = '<.........>'
        return (

            <View style={{ flex: 1, margin: 20 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Tue. 19jan -12:20
                       </Text>
                            <Text>
                                Avenue Elizabeth city{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Bethesda MD
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>Completed</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Wed. 20jan -16:00
                       </Text>
                            <Text>
                                Bethesda MD{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Avenue Elizabeth city
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>Completed</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View><View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Thus. 21jan -18:00
                       </Text>
                            <Text>
                                Avenue Elizabeth city{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Bethesda MD
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>Completed</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* <View style={{marginTop:30}}>
                        <Button title="SCHEDULE A RIDE " color="#636FA4"/>
                    </View> */}
                </ScrollView>
            </View>
        )
    }
}
