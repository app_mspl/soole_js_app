import React, { Component } from 'react'
import { Text, View,SafeAreaView } from 'react-native'
import {createMaterialTopTabNavigator,createAppContainer} from 'react-navigation'
import Upcoming from './UpcomingRide'
import Compteted from './CompletedRide'
import Styles from '../../Stylesheet';
const RideTab=createMaterialTopTabNavigator({
Upcoming:{
    screen:Upcoming
},
Compteted:{
    screen:Compteted
}
},
{
    tabBarOptions:{
        activeTintColor: '#636FA4',
        inactiveTintColor:'gray',
        indicatorStyle:{
            backgroundColor:'#636FA4'
        },
        
        style: {
            backgroundColor: '#fff',
          },
    }
}
)
const RideTab1=createAppContainer(RideTab)
export default class MyRides extends Component {
  render() {
    return (
      <SafeAreaView style={{flex:1}}>
         <View style={Styles.header}>
                    <Text style={{ color: '#fff', fontWeight: '500' }}>My Rides</Text>
                </View>
                <RideTab1/>
      </SafeAreaView>
    )
  }
}
