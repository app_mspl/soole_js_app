import React, { Component } from 'react'
import { Alert,Text, View, ScrollView, TouchableOpacity, Button, RefreshControl,FlatList,ActivityIndicator,SafeAreaView } from 'react-native'
import Styles from '../../Stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
const STATE_PENDING = 0;

const STATE_DECLINED = 1;

const STATE_CANCELLED = 2;

const STATE_COMPLETED = 3;

const STATE_REQUESTED = 4;

export default class RideHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            route: null,
            time: null,
            seats: null,
            ridestatus: null,
            ride_history: null,
            refreshing: false,

        }
    }
    _onRefresh = () => {
        this.setState({ refreshing: true });
        this._RideHistory();


    }
    componentDidMount() {
        this._RideHistory();
    }
    _RideHistory() {
        this.setState({ refreshing: false });
        url = global.ConstUrl + 'api/my-rides/'+ global.Id
        fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': global.BarCode
            },
        })
            .then(response => response.json())
            
            .catch(error => {
                this.setState({
                    animate: false
                })
                Alert.alert('Please Check Your Internet connection')
            })
            .then(response => {
                if (response.success == true) {
                    this.setState({ ride_history: response.data, })

                }
                else {

                }
            })

    }
    keyExtractor = (item, index) => {
    console.warn(item.id)
        return item.id.toString();
      }
    renderItem = ({ item }) => {
        <View style={{ flex: 1,backgroundColor:"#fff" }}>
        
        {console.warn(item)}
        <Text style={{}}>No History Available</Text>
            {/* <View style={Styles.RideView}>
                <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                        {item.start_time}
                       </Text>
                    <Text>
                        {item.route_title}
                       </Text>
                </View>
                <View style={{ padding: 10, flexDirection: 'row' }}>
                    <Text style={{ flex: 1, }}>Completed</Text>
                    <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                        <FontAwesome name="ellipsis-v" size={20} color="#000" />
                    </TouchableOpacity>
                </View>
            </View> */}
        </View>


    }

    render() {
        const pattren = '<.........>'
        const Ride_history=this.state.ride_history
        if (!Ride_history) {
          return (
            <ActivityIndicator
              animating={true}
              style={Styles.indicator}
              size='large'
            />
          );
        }
        return (
            <SafeAreaView style={{flex:1,margin:20,backgroundColor:"blue"}}>
            <FlatList
            // horizontal={true}
             refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
              extraData={this.state}
              data={Ride_history}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
            />
            </SafeAreaView>
                /* <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Tue. 22jan -12:20
                       </Text>
                            <Text>
                                Avenue Elizabeth city{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Bethesda MD
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>Completed</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Wed. 23jan -16:00
                       </Text>
                            <Text>
                                Bethesda MD{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Avenue Elizabeth city
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>Completed</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.RideView}>
                        <View style={{ padding: 10, borderBottomWidth: 1, borderColor: '#d3d3d3', paddingBottom: 30 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                Thus. 24jan -18:00
                       </Text>
                            <Text>
                                Avenue Elizabeth city{<Text style={{ fontSize: 10, alignItems: 'flex-end' }}>{pattren}</Text>}Bethesda MD
                       </Text>
                        </View>
                        <View style={{ padding: 10, flexDirection: 'row' }}>
                            <Text style={{ flex: 1, }}>Completed</Text>
                            <TouchableOpacity style={{}} onPress={() => alert('pressed')}>
                                <FontAwesome name="ellipsis-v" size={20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* <View style={{marginTop:30}}>
                        <Button title="SCHEDULE A RIDE " color="#636FA4"/>
                    </View> *
                </ScrollView> */
           
        )
    }
}
