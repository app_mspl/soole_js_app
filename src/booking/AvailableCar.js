import React, { Component } from 'react'
import { Text, View, SafeAreaView, ScrollView, TextInput, Image, Dimensions, TouchableOpacity } from 'react-native'
import Styles from '../../Stylesheet';
import MapComponent from '../Gmap/GoogleMap';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
const { width, height } = Dimensions.get('window')
export default class AvailableCar extends Component {

    constructor(props) {
        super(props);

    }
    render() {
        return (

            <SafeAreaView style={{ flex: 1 }}>
                <View style={Styles.HeaderRide}>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.props.navigation.openDrawer()}>
                        <FontAwesome name="bars" size={30} color="#fff" />
                    </TouchableOpacity>
                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 2, }}>
                        <Text style={{ color: '#fff', fontWeight: '500', }}>
                            Pick Car</Text>
                    </View>
                    <View style={{ flex: 1 }} />
                </View>
                <View style={{ padding: 30 }}>
                    <View style={Styles.ProfileDetail}>
                        <Text style={Styles.Name}>Pic Up Address</Text>
                        <Text>test</Text>
                    </View>
                    <View style={Styles.ProfileDetail}>
                        <Text style={Styles.Name}>Drop Address</Text>
                        <Text>test</Text>
                    </View>
                    <View style={Styles.ProfileDetail}>
                        <Text style={Styles.Name}>Seat</Text>
                        <Text>3</Text>
                    </View>

                    <View style={Styles.ProfileDetail}>
                        <Text style={Styles.Name}>Ride At</Text>
                        <Text>10:00:am</Text>
                    </View>
                    <View style={Styles.ProfileDetail}>
                        <Text style={Styles.Name}>Payment Method</Text>
                        <Text>Online</Text>
                    </View>
                    <View style={Styles.ProfileDetail}>
                        <Text style={Styles.Name}>Driver Detail</Text>
                        <Text>test</Text>
                        <Text>11023456987</Text>

                    </View>

                </View>
                {/* <Image source={require('../../img/assets/cars.png')} style={{ width: width, height: 50, marginTop: 250 }} /> */}
                <View style={{ height: 100, backgroundColor: '#fff', flexDirection: 'row', marginLeft: 10, marginRight: 10, }}>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'center', }}>
                        <Text>08:00 AM</Text>
                        <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/30/000000/sedan.png' }} style={{ height: 40, width: 40 }} />
                        <TouchableOpacity style={[Styles.AJ, { height: 30, backgroundColor: '#636FA4', width: "100%" }]}>
                            <Text style={{ color: '#fff', fontWeight: '500' }}>John</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'center' }}>
                        <Text>10:00 AM</Text>
                        <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/30/000000/sedan.png' }} style={{ height: 40, width: 40 }} />
                        <TouchableOpacity style={[Styles.AJ, { height: 30, backgroundColor: '#fff', width: "100%" }]}>
                            <Text style={{ fontWeight: '500' }}>Depp</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'center' }}>
                        <Text>12:00 PM</Text>
                        <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/30/000000/sedan.png' }} style={{ height: 40, width: 40 }} />
                        <TouchableOpacity style={[Styles.AJ, { height: 30, backgroundColor: '#fff', width: "100%" }]}>
                            <Text style={{ fontWeight: '500' }}>Alfando</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'center' }}>
                        <Text>No Cab</Text>
                        <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/30/000000/sedan.png' }} style={{ height: 40, width: 40, opacity: 0.3 }} />
                        <TouchableOpacity style={[Styles.AJ, { height: 30, backgroundColor: '#fff', width: "100%" }]}>
                            <Text style={{ fontWeight: '500' }}>Michale</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'center' }}>
                        <Text>04:00 PM</Text>
                        <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/30/000000/sedan.png' }} style={{ height: 40, width: 40 }} />
                        <TouchableOpacity style={[Styles.AJ, { height: 30, backgroundColor: '#fff', width: "100%" }]}>
                            <Text style={{ fontWeight: '500' }}>sam</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>

                </View>

            </SafeAreaView>
        )
    }
}
