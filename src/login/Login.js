import React, { Component } from 'react'
import { Alert, Text, View, SafeAreaView, StatusBar, TextInput, TouchableOpacity, Button, BackHandler, Modal, ActivityIndicator } from 'react-native'
import Styles from '../../Stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import LinearGradient from 'react-native-linear-gradient'
import Logo from '../Logo'
import HandleBack from '../../HandleBack';
import { AsyncStorage } from 'react-native';




global.ColorG = ['#E8CBC0', '#636FA4']
global.ConstUrl = 'http://93.188.167.68/projects/soolerides/public/'
global.BarCode = 'Bearer  eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImY4YjlhZjUzMzE2ZGM0MmY0OWJmOTUwN2RjYzY3YzAyYTgxMTU1M2M2YWE0YTZlZTI0YjI4MTQxNmU0ZGQwNmZhZGRhMTZjOTBjNWY2MDhkIn0.eyJhdWQiOiIyIiwianRpIjoiZjhiOWFmNTMzMTZkYzQyZjQ5YmY5NTA3ZGNjNjdjMDJhODExNTUzYzZhYTRhNmVlMjRiMjgxNDE2ZTRkZDA2ZmFkZGExNmM5MGM1ZjYwOGQiLCJpYXQiOjE1NDg5MjA3NzksIm5iZiI6MTU0ODkyMDc3OSwiZXhwIjoxNTgwNDU2Nzc5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.M8qMDt9oMg1LyHIvLmFy2uLz40Dg5Sr6eKXYAdwGeDI0oUKluacYdoJuqyrnjOiQqDByARfeaQmao-Z_I70ez3_9Lfb2LFAxMkeRtztrLekyJiUbzJmVHiG_79VYZ1YmJz40Z5qQKPg99pMvMLS3M2GLG6oN9rGiepbKG-kKJbhMzMEcbniCylPNCES3RoUlOpqb3uIPFieWdHk6EUtjcMtSnrCt4juiGSOs2e8g1GCvwzXyMmpzAiA74N0PfM8BjHq6SnRULM0R_dOXreyAAnUzFGduKOKcD_0X_exHtzLcxJvPGUE1Qg7ZW10c94pwIgSHF0m5q1cmUTYdq2BytxgDd-hPBUnfP1fBWg2epYc3R_jznTdLjTH12FpwlsRw38t0XHTEjqx1k8YI2tj4GJzpQ-ceAgD5nQiHT3kObWZ-s7_cB-vxHc21uj6fpzkyKxTJzGylOwKnlnFMK72agAe-KxV4ZePp9CInxE5Awcp38hsNZEsX0TjxyegO4yD3lzbZ6a_O1r5XEyMbGUzsj5IUolNnK7qIAzL_w0a0M1uuT7bEvJlySNIgI78P9T7F8kNZaqIXNtxKhZFhUNuZ0RmjVm6o__Zhxwys-6R-6pmMLamUTQ2wFBsgYnf1wLcvyVy1A8blgQJA4PbsUwl0hTMhpEsV3uGcnYiuaviutzo'
global.any = ""
export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            borderWuser: 0,
            borderWpass: 0,
            animate: false,



        }
    }
    LoginFailed = () => {
        Alert.alert(
            "Login Failed",
            " Please Check your Email or Password",
            [
                { text: "OK", },
            ],
            { cancelable: false },
        );
        return true;

    }
    onBack = () => {
        Alert.alert(
            "",
            "Do You Want to Quit The App?",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: "Yes", onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        );
        return true;

    }


    render() {
        return (
            <HandleBack onBack={this.onBack} style={{ flex: 1 }}>

                <SafeAreaView style={{ flex: 1 }}>
                    <StatusBar backgroundColor="#E8CBC0" barStyle="light-content" />
                    <LinearGradient colors={global.ColorG} style={[{ flex: 1 }]}>

                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Logo />

                        </View>
                        <View style={{ flex: 2, }}>
                            <View>
                                <View style={{ marginLeft: 20, marginRight: 20 }}>
                                    <Text style={Styles.LoginLabel}>EMAIL ADDRESS</Text>
                                    <TextInput
                                        placeholder='Email address'
                                        onChangeText={(email) => this.setState({ email })}
                                        style={[Styles.LoginText, { borderWidth: this.state.borderWuser, borderColor: 'red' }]}
                                    />
                                    {/* <Text style={{marginLeft:20,color:'red',fontWeight:'500'}}>*Required</Text> */}
                                </View>
                                <View style={{ marginLeft: 20, marginRight: 20 }}>
                                    <Text style={Styles.LoginLabel}>PASSWORD</Text>
                                    <TextInput
                                        placeholder='Password'
                                        onChangeText={(password) => this.setState({ password })}
                                        secureTextEntry
                                        style={[Styles.LoginText, { borderWidth: this.state.borderWpass, borderColor: 'red' }]}
                                    />
                                </View>
                            </View>

                            <TouchableOpacity style={[Styles.LoginBtn, Styles.AJ]} onPress={this.login}>
                                <Text style={Styles.LoginBtnTxt}>LOGIN</Text>
                            </TouchableOpacity>
                            <View style={{ flex: 1, flexDirection: 'row', margin: 20 }}>
                                <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                    <Text style={{ color: '#fff', textDecorationLine: 'underline' }}>
                                        Forget Password</Text>
                                </TouchableOpacity>
                                <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: "row" }}>
                                    <Text style={{ color: '#fff' }}>
                                        New user?</Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
                                        <Text style={{ color: '#fff' }}> Signup</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                        <Modal visible={this.state.animate} transparent={true} >
                            <View style={{
                                flex: 1,
                                justifyContent: 'center',
                            }}>
                                <View style={Styles.ModalView}>
                                    <ActivityIndicator
                                        animating={this.state.animate}
                                        // style={Styles.indicator}
                                        style={{ justifyContent: 'center' }}
                                        size='large'
                                        color="#fff"
                                    />
                                    <View style={Styles.MV2}>
                                        <Text style={Styles.MVtext}>Please wait...</Text>
                                    </View>
                                </View>
                            </View>

                        </Modal>
                    </LinearGradient>

                </SafeAreaView>
            </HandleBack>

        )
    }
    login = () => {

        email = this.state.email
        password = this.state.password
        url = global.ConstUrl + 'api/login'
        if (!email == '' && !password == '') {
            this.setState({
                animate: true,
                borderWuser: 0,
                borderWpass: 0,

            })
            var formdata = new FormData();
            formdata.append('email', email);
            formdata.append('password', password);
            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': global.BarCode
                },
                body: formdata
            }).then(response => response.json())
                .then(response => {
                    if (response.status == 200) {
                        this.setState({
                            animate: false
                        })
                        AsyncStorage.setItem('userDetail', JSON.stringify(response.details));
                        global.Id = response.details.id
                        global.Img = response.details.image_file
                        global.name = response.details.name
                        global.email = response.details.email
                        global.roleId = response.details.role_id
                        global.driverMode = response.details.driver_mode
                        global.phoneNo = response.details.phone
                        global.Profile = global.ConstUrl + 'images/' + global.Img
                        this.props.navigation.navigate('DrawerAuth')
                    }
                    else {
                        this.setState({
                            animate: false
                        })
                        this.LoginFailed()
                    }
                })
                .catch(error => console.error('Error:', error));
            // .then(response => response.json())
            // .catch(error => {
            //     this.setState({
            //         animate: false
            //     })
            //     Alert.alert('Please Check Your Internet connection')
            // })
            // .then(response => {
            //     console.warn('jsonrespose', response)
            //     let data = response.JSON.parse()
            //     console.warn('realobject', data)

            // })


        } else {
            if (!email == '') {
                this.setState({ borderWuser: 0 })


            } else {
                this.setState({ borderWuser: 2 })
            }
            if (!password == '') {
                this.setState({ borderWpass: 0 })

            } else {
                this.setState({ borderWpass: 2 })
            }
        }


    }

}
