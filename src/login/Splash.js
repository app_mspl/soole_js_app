import React, { Component } from 'react'
import { Text, View, SafeAreaView, Image, StatusBar } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Styles from '../../Stylesheet'
import Logo from '../Logo'
import { AsyncStorage } from 'react-native';
export default class Splash extends Component {
    componentDidMount() {
        // Start countithisng when the page is loaded
        this.retrieveData()
        this.timeoutHandle = setTimeout(() => {

        }, 2000);
    }
    retrieveData = async () => {
        try {
            var data = await AsyncStorage.getItem('userDetail');
            console.log('stringyfyData', data)
            var userDetail = JSON.parse(data);
            console.log('userDetail', userDetail)
            if (userDetail !== null) {
                this.props.navigation.navigate('DrawerAuth')
            }
            else {
                this.props.navigation.navigate('LoginStack')
            }
        } catch (error) {
            // Error retrieving data
        }
    };
    componentWillUnmount() {
        clearTimeout(this.timeoutHandle); // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <LinearGradient colors={global.ColorG} style={[Styles.AJ, { flex: 1 }]}>
                    <StatusBar backgroundColor="#E8CBC0" barStyle="light-content" />
                    <Logo />
                </LinearGradient>
            </SafeAreaView>
        )
    }
}
