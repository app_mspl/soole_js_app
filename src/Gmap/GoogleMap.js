import React, { Component } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
// import { GooglePlacesAutocomplete1 } from 'react-native-google-places-autocomplete'
import {
  View,
  StyleSheet,
  Dimensions, Image, Text, ScrollView, ActivityIndicator, TouchableOpacity
} from 'react-native';
import MapViewDirections from 'react-native-google-maps-directions'
import MapView, { Marker, Callout } from 'react-native-maps';
import Styles from '../../Stylesheet'
const { width, height } = Dimensions.get('window')
import RetroMapStyle from './RetroMapStyle.json'
import { Button } from 'react-native-elements';
const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

class MapComponent extends Component {

  constructor() {

    super()
    this.state = {
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },

    }
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition((position) => {
      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)

      var initialRegion = {
        latitude: lat,
        longitude: long,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }


      this.setState({ initialPosition: initialRegion })
    },
      (error) => alert(JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, });
  }



  renderScreen = () => {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          // customMapStyle={RetroMapStyle}
          // region={this.state.initialPosition}
          showsUserLocation={true}
          initialRegion={this.state.initialPosition}
        // onRegionChange={this.onRegionChange}
        >
          <MapView.Marker
            coordinate={this.state.initialPosition}
            ref={_marker => {
              this.marker = _marker;
            }}
            onPress={() => { }}
            onCalloutPress={() => {
              this.marker.hideCallout();
            }}>
            <Image
              source={{ uri: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png' }}
              style={{ width: 60, height: 60 }}
            />
            <View>
              <Text>
                marker text
          </Text>
            </View>
            <MapView.Callout
              tooltip={true}>
              <View>
                <Text>


                  callout text
                </Text>
              </View>
            </MapView.Callout>
          </MapView.Marker>
        </MapView>
        <View>
          <TouchableOpacity style={{ width: 52, height: 52, backgroundColor: 'pink' }}>
            <Text>ok</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 10 }}>

          <GooglePlacesAutocomplete
            placeholder='Enter pickup Location'
            minLength={4}
            autoFocus={false}
            returnKeyType={'default'}
            fetchDetails={true}
            query={{
              key: 'AIzaSyDBDBOnX3yi1xT8UFDYQ6i7kDTU4PkYOXU',
              language: 'en', // language of the results
              types: '(regions)' // default: 'geocode'
            }}
            onPress={(data, details = null) => {

              var data = {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              };
              // this.setState({initialPosition: data});
              // console.warn(this.state.initialPosition.latitude)
            }}
            styles={{
              textInputContainer: {
                backgroundColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth: 0,
                width: 300
              },
              textInput: {
                placeholder: "Enter Pickup Location",
                marginLeft: 0,
                marginRight: 0,
                height: 40,
                color: '#5d5d5d',
                fontSize: 16,
                width: 300
              },
              predefinedPlacesDescription: {
                color: '#1faadb'
              },
            }}
            currentLocation={false}
          />
          <GooglePlacesAutocomplete
            placeholder='Enter Drop Location'
            minLength={2}
            autoFocus={false}
            returnKeyType={'default'}
            fetchDetails={true}
            query={{
              key: 'AIzaSyDBDBOnX3yi1xT8UFDYQ6i7kDTU4PkYOXU',
              language: 'en', // language of the results
              types: '(regions)' // default: 'geocode'
            }}
            onPress={(data, details = null) => {

              var data = {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              };
              // this.setState({initialPosition: data});
              // console.warn(this.state.initialPosition.latitude)
            }}
            styles={{
              textInputContainer: {
                backgroundColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth: 0,
                width: 300
              },
              textInput: {
                placeholder: "Enter Drop Location",
                marginLeft: 0,
                marginRight: 0,
                height: 40,
                color: '#5d5d5d',
                fontSize: 16,
                width: 300
              },
              predefinedPlacesDescription: {
                color: '#1faadb'
              },
            }}
            currentLocation={false}
          />

        </View>

        {/* <View style={{ marginTop: 10 }}>

          <GooglePlacesAutocomplete
            placeholder='Enter Drop Location'
            minLength={2}
            autoFocus={false}
            returnKeyType={'default'}
            fetchDetails={true}
            query={{
              key: 'AIzaSyDBDBOnX3yi1xT8UFDYQ6i7kDTU4PkYOXU',
              language: 'en', // language of the results
              types: '(regions)' // default: 'geocode'
            }}
            onPress={(data, details = null) => {

              var data = {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              };
              // this.setState({initialPosition: data});
              // console.warn(this.state.initialPosition.latitude)
            }}
            styles={{
              textInputContainer: {
                backgroundColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth: 0,
                width: 300
              },
              textInput: {
                // placeholder:"Enter Pickup Location",
                marginLeft: 0,
                marginRight: 0,
                height: 40,
                color: '#5d5d5d',
                fontSize: 16,
                width: 300
              },
              predefinedPlacesDescription: {
                color: '#1faadb'
              },
            }}
            currentLocation={false}
          />

        </View> */}



      </View>
    );
  }

  render() {
    const Pro = this.state.initialPosition
    if (!Pro) {
      return (
        <ActivityIndicator
          animating={true}
          style={Styles.indicator}
          size='large'
        />
      );
    }
    return (
      this.renderScreen()
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default MapComponent;