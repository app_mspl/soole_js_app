
import {
    Dimensions,
    StyleSheet,
} from 'react-native';
const window = Dimensions.get('window');

const Styles = StyleSheet.create({
    container: {
        overflow: 'hidden',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        paddingLeft: 20,
        height: 80,
        backgroundColor: '#636FA4',
        justifyContent: 'center',
    },
    hiddenContainer: {
        top: window.height,
        bottom: -window.height
    },
    LoginText: {
        height: 50,
        backgroundColor: '#fff',
        elevation: 10,
        borderRadius: 10,
        paddingLeft: 10
    },
    LoginLabel: {
        color: '#fff',
        marginLeft: 10,
        marginBottom: 10,
        marginTop: 20
    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtn: {
        height: 50,
        backgroundColor: '#F7000C',
        marginTop: 30,
        elevation: 10,
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 10
    },
    ChangePassBtn: {
        height: 50,
        backgroundColor: '#F7000C',
        marginTop: 30,
        elevation: 10,
        borderRadius: 10
    },
    LoginBtnTxt: {
        color: '#fff',
        fontSize: 15
    },
    ForgetPassDis: {
        color: '#fff',
        marginTop: 5,
        marginLeft: 70,
        marginRight: 70,
        textAlign: 'center'
    },
    ForgetPassLog: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    RatingTxt: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#fff'
    },
    ProfileDetail: {
        justifyContent: 'center',
        paddingLeft: 10,
        backgroundColor: '#fff',
        height: 60,
        marginBottom: 10,
        elevation: 5,
        borderRadius: 2,
    },
    ProfileHeader: {
        paddingLeft: 20,
        height: 80,
        justifyContent: 'center',
        backgroundColor: '#636FA4'
    },
    ProfileName: {
        fontWeight: '500',
        color: '#fff',
        fontSize: 18,
    },
    ListItem: {
        flexDirection: 'row',
        margin: 10,
        alignItems: 'center',
        height: 50,
    },
    Name: {
        color: '#636FA4',
        fontWeight: '500',
    },
    RideView: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#d3d3d3',
        marginBottom: 10
    },
    FindRideView: {
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    MapoverView: {
        flexDirection: 'row',
        height: 40,
        backgroundColor: '#fff',
        marginTop: 20,
        marginLeft: 30,
        marginRight: 30,
        alignItems: 'center',
    },
    indicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    FaqQst: {
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10,
    },
    HeaderRide: {
        height: 80,
        backgroundColor: '#636FA4',
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 20,
    },
    ModalView: {
        backgroundColor: '#636FA4',
        height: 80,
        borderRadius: 3,
        flexDirection: 'row',
        paddingLeft: 20,
        marginLeft: 50,
        marginRight: 50,
        elevation: 40,
    },
    MV2: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: 30,
    },
    MVtext: {
        color: '#fff',
        fontWeight: '500',
    },
    DateTime: {
        height: 30,
        flex: 1,
        // borderBottomWidth:1,
        borderRadius: 5,
        margin: 10,
    },

})
export default Styles