import React, { Component } from 'react'

import {
    Dimensions,
    StyleSheet,
    Text,
    View,
} from 'react-native';

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        overflow: 'hidden',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
        // flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    // This pushes the view out of the viewport, but why the negative bottom?
    hiddenContainer: {
        top: window.height,
        bottom: -window.height
    }
});

class Test extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={[styles.container, { backgroundColor: 'red' }, styles.hiddenContainer]}>
                    <Text>Can see me!</Text>
                </View>
                <View style={[styles.container, { backgroundColor: 'yellow' },]}>
                    <Text>Cannot see me</Text>
                </View>
                <View style={[styles.container, { backgroundColor: 'blue' }, styles.hiddenContainer]}>
                    <Text>Cannot see me</Text>
                </View>
            </View>
        );
    }
}

export default Test;